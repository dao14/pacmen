# Pacmen

Pacmen is a cpp shell for the arch linux package manager pacman.
It aims to be easy to use, yet supply all the necessary functionality.

## Installation

Pacmen is designed for a arch linux system.

Clone the souce code and move into the directory.

$ git clone https://gitlab.com/dao14/pacmen.git

$ cd pacmen

Make the installer executable and execute it.

$ chmod +x install

$ ./install
