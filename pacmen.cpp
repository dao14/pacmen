#include <iostream>
#include <string>


int main() ;


class pacmen {

	private :
		std::string conf_cmd = "sudo nvim /etc/pacman.conf" ;
		std::string install_cmd = "sudo pacman -S" ;
		std::string search_cmd = "pacman -Ss" ;
		std::string update_cmd = "sudo pacman -Syyu --noconfirm" ;
	public :
		void conf() ;
		void install( std::string package ) ;
		void search( std::string package ) ;
		void update() ;
} ;

void pacmen::conf()
{
	std::system( conf_cmd.c_str() ) ;
}
void pacmen::install( std::string package )
{
	std::system( ( install_cmd + " " + package ).c_str() ) ;
}
void pacmen::search( std::string package )
{
	std::system( ( search_cmd + " " + package ).c_str() ) ;
}
void pacmen::update()
{
	std::system( update_cmd.c_str() ) ;
}


class pacmen_io {
	private :
		std::string prefix = ">> " ;
		std::string modes_msg = "conf / install / search / update / quit" ;
		std::string package_msg = "package" ;
	public :
		void output( std::string msg ) ;
		void mode( std::string &mode ) ;
		std::string package() ;
} ;

void pacmen_io::output( std::string msg )
{
	std::cout << prefix << msg << "\n" ;
	std::cout << prefix ;
}
void pacmen_io::mode( std::string &mode )
{
	output( modes_msg ) ;
	std::cin >> mode ;
}
std::string pacmen_io::package()
{
	std::string pkg ;

	output( package_msg ) ;
	std::cin >> pkg ;

	return pkg ;
}


int main()
{
	pacmen pac ;
	pacmen_io io ;
	std::string mode ;

	for ( int i = 0 ; i < 100 ; i++ ) {
		io.mode( mode ) ;

		if ( mode == "conf" ) {
			pac.conf() ;
		} else if ( mode == "install" ) {
			pac.install( io.package() ) ;
		} else if ( mode == "search" ) {
			pac.search( io.package() ) ;
		} else if ( mode == "update" ) {
			pac.update() ;
		} else if ( mode == "quit" ) {
			break ;
		} else {
			std::cout << ">> Mode not found!" << std::endl ;
		}
	}
}
